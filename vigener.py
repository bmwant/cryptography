import string

from itertools import cycle
from functools import reduce

# letters case-insensitive, digits and space
ALPHA = string.ascii_letters + string.digits + ' '
ALPHA_LEN = len(ALPHA)


def encrypt(key, plaintext):
    """Encrypt the string and return the ciphertext"""
    pairs = zip(plaintext, cycle(key))
    result = ''

    for pair in pairs:
        index_sum = lambda x, y: ALPHA.index(x) + ALPHA.index(y)
        total = index_sum(*pair)
        result += ALPHA[total % ALPHA_LEN]

    return result


def decrypt(key, ciphertext):
    """Decrypt the string and return the plaintext"""
    pairs = zip(ciphertext, cycle(key))
    result = ''

    for pair in pairs:
        total = reduce(lambda x, y: ALPHA.index(x) - ALPHA.index(y), pair)
        result += ALPHA[total % ALPHA_LEN]

    return result


def check_data(input_text, what='Some data'):
    """Check if input data corsponds to our alphabet"""
    flag = False
    for char in input_text:
        if char not in ALPHA:
            print('%s contains symbol "%s" that is not in alphabet' % (what, char))
            flag = True
    if flag:
        raise ValueError('Check your input')


PLAINTEXT = 'Jamaica the jam'
KEY = 'Kioto the key'


if __name__ == '__main__':
    check_data(PLAINTEXT, 'Input text')
    check_data(KEY, 'Key')
    encrypted = encrypt(KEY, PLAINTEXT)
    decrypted = decrypt(KEY, encrypted)

    print('Key: %s' % KEY)
    print('Plaintext: %s' % PLAINTEXT)
    print('Encrypted: %s' % encrypted)
    print('Decrypted: %s' % decrypted)
